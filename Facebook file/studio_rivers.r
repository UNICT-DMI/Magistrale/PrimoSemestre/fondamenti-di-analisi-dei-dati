options(digit=3);
primo_q <- as.numeric(quantile(rivers, probs=0.25));
terzo_q <- as.numeric(quantile(rivers, probs=0.75));
mediana <- median(rivers);


#Studio dei dati di rivers (che raccoglie la lunghezza dei fiumi importanti dell'America del Nord)
#Mostra la sintesi a 5 + 1 di rivers(cioè mostra il min, il primo quartile, la mediana, il terzo quartile, il massimo e la media di rivers)
print("Studio dei dati contenuti in rivers: ")
print("Panoramica dei dati di rivers: ")
print(summary(rivers));
#viene mostrato il campo di varianza interquartile(IQR). Misura la dispersione dei dati
#escludendo i valori estremi dell'intervallo(in modo da non trovare outlier)

cat("L'IQR dei dati di rivers è: ");
cat(IQR(rivers));

boxplot(rivers)
print("La varianza di rivers è: ")
print(var(rivers));
print("Lo scarto quadratico di rivers è: ");
print(sd(rivers));
print("Vengono stampati gli outlier di rivers: ");
print(rivers[scale(rivers) < -3 | scale(rivers) > 3]);

if((mediana - primo_q) > (terzo_q - mediana))
{
  print("I dati sono assimetrici a sinistra");
  print("Verificare il boxplot per verificare");
}
 
if((mediana - primo_q) < (terzo_q - mediana))
{
  print("I dati sono assimetrici a destra");
  print("Verificare il boxplot per verificare");
}

