
# Caricare i dati
data("mtcars")

#-----------------------------------------------------

# Questo data set riporta informazioni su diversi modelli di automobili.
# Useremo solo le colonne 1 (miglia per gallone) , 2 (n.cilindri), 6 (peso), 10 (numero di marce)
D <- data.frame(a=mtcars[,1],b=mtcars[,2],c=mtcars[,6],d=mtcars[,10])

#-----------------------------------------------------

# relazione  tra le colonne 1 e 6.
# Calcolare l'indice di correlazione di Pearson tra queste due colonne.
# Calcolare una regressione lineare tra queste colonne.
# Illustrare con un plot arricchito dalla retta di regressione la relazione trovata.
# E' una relazione significativa?

corrPearson_ac <- cor(D$a, D$c, method = "pearson")

plot(D$c, D$a)

linearRegr <- lm(D$a ~ D$c)

abline(linearRegr$coefficients, col="red", lwd=2)
# abbiamo una correlazione negativa abbastanza significativa

# il successivo esercizio chiede di "predire" i valori della colonna 2
# o di tentare di calssificare le macchine seconod i valori nella colonna 2
# ma quali osno questi valori?
H=hist(D$b) # grafica
print(H$counts)

# conviene trasformare i valori 4, 6 e 8 in valori "categoriali"
D$b<-factor(D$b,labels=c("1","2","3"))
summary(D)
# ci dice che il range dei valori nelle colonne da usare per classificare
# � troppo diverso da colonna a colonna
# un metodo "metrico" come il k-nn richiede una normalizzazione
# va benissimo il solito "scale" di R
D$a<-scale(D$a)
D$c<-scale(D$c)
D$d<-scale(D$d)

library(class) # libreria necessaria per il knn
library(rpart) # libreria necessaria per l'albero decisionale
# ciclo di ripetizione per il knn
hits1=0
hits3=0
hits5=0
hitsTree=0
N_ripetizioni=100
for (i in seq(1:N_ripetizioni)) {
  inTS<-sample(1:nrow(D), size = nrow(D)/2, replace = FALSE) # estrazione TS
  TS<-D[inTS,]
  CS=D[-inTS,]
  knn1 <- knn(TS[,c(1,3,4)], CS[,c(1,3,4)], TS[,2], k=1) # classificazione
  hits1=hits1+sum(knn1==CS[,2])
  knn3 <- knn(TS[,c(1,3,4)], CS[,c(1,3,4)], TS[,2], k=3) # classificazione
  hits3=hits3+sum(knn3==CS[,2])
  knn5 <- knn(TS[,c(1,3,4)], CS[,c(1,3,4)], TS[,2], k=5) # classificazione
  hits5=hits5+sum(knn5==CS[,2])
  albero<-rpart(b ~., data = TS, method = "class")
  predizione <- predict(albero, CS, type = "class")
  hitsTree=hitsTree+sum(predizione==CS[,2])
}
hitsMedi=(c(hits1,hits3,hits5,hitsTree)/N_ripetizioni)
print(hitsMedi/16) # normalizzati con la numerosit� del CS