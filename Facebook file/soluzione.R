# compito marzo 2016

# importare i dati "attitude"

data("attitude")
summary(attitude)

# non necessario am semplifica il codice successivo

attach(attitude)

# non richiesto am utile a capire
plot(attitude)
boxplot(attitude)
# punto 1
# tradurre i dati di gradimento in 1 per sopra la media e 0 sotto la media

m=mean(rating)
attitude$R=(rating>m)*1

# punto 2
# calcolare l'indice di Pearson tra la prima colonna (rating) e le altre colonne
# del DB
# attenzione la correlazione va calcolata cn l'originale colonna "rating"

pc = cor(rating, attitude[,2:7],method = "pearson")

cat("la correlazione di Pearson tra 'rating' e le altre colonne �:\n")
cat(pc)
cat("\n")
cat("\n")

# punto 3
# calcolare l'indice di Spearman tra la prima colonna (rating) e le altre colonne
# del DB
# attenzione la correlazione va calcolata cn l'originale colonna "rating"

sc = cor(rating, attitude[,2:7],method = "spearman")

cat("la correlazione di Spearman tra 'rating' e le altre colonne �:\n")
cat(sc)
cat("\n")
cat("\n")

# punto 4
# trovare le colonne meglio correlate con la prima
# sia per Pearson che per Spearman sono le prime tre
# la soluzione si trova per ispezione diretta delle due sequenze di indici appena calcolate

# punto 5
# classificazione per la colonna R
# ottenuta studiando le colonne 2, 3 e 4
# secondo il metodo del discriminante di Fisher

# estrazione di 20 dipartimenti a caso per il TS
TS_index=sample(30,20,replace=FALSE)
CS_index=setdiff(1:30,TS_index) # calcolo degli indici "complementari" al TS

TS=attitude[TS_index,2:4]
TS$R=attitude$R[TS_index]

CS=attitude[CS_index,2:4]
CS$R=attitude$R[CS_index]

# formazione degli insiemi di dipartimenti in TS con R==0 e con R==1
TS0=TS[TS$R==0,1:3]
TS1=TS[TS$R==1,1:3]

# di seguito il calcolo diretto del discriminate di Fisher
# si poteva usare la funzione predefinita in R "lda"

m0=c(mean(TS0[,1]), mean(TS0[,2]), mean(TS0[,3]))
m1=c(mean(TS1[,1]), mean(TS1[,2]), mean(TS1[,3]))
M=m1-m0

S0=cov(TS0[,1:3])
S1=cov(TS1[,1:3])
sigmaInverse=solve(S0+S1)

w=M %*% sigmaInverse

cat("i 'pesi' w del discriminante di Fisher sono \n")
cat(w)
cat("\n")

# calcolo del valore di separazione tra i due gruppi
soglia=sum((1/2)*w*(m0+m1))

# punto 6 matrice di confusione del TS
# calcolo delle etichette predette per le osservazioni nel TS
predizione=(w[1]*TS[,1]+w[2]*TS[,2]+w[3]*TS[,3]>soglia)*1


cat("matrice di confusione sul training set usando tre colonne \n")
print(table(TS$R,predizione))
cat("\n")

# punto 7 matrice di confusione sul CS
# calcolo delle etichette predette per le osservazioni nel CS
predizione=(w[1]*CS[,1]+w[2]*CS[,2]+w[3]*CS[,3]>soglia)*1

cat("matrice di confusione sul control set usando tre colonne \n")
print(table(CS$R,predizione))
cat("\n")

# punto 8 calcolare il Discriminante di Fosher usando TUTTE le colonne
# si pu� generalizzare il calcolo precedente
# ma per illustrare l'uso della fuznione lda usiamo tale funzione
library(MASS)

TS2=attitude[TS_index,2:8]
CS2=attitude[CS_index,2:8]

discriminante=lda(TS2$R~.,data=TS2)
predizione1=predict(discriminante, TS2)

# punto 9
cat("matrice di confusione sul training set con tutte le colonne \n")
print(table(TS2$R,predizione1$class))
cat("\n")

# punto 10
predizione1=predict(discriminante, CS2)
cat("matrice di confusione sul training set con tutte le colonne \n")
print(table(CS2$R,predizione1$class))
cat("\n")

# punto 11
# eseguendo il test pi� volte si vede che i risultati ottenuti tre colonne
# sono molto simili aquelli ottenuti usando tutte e colonne
# infatti l'aggiunta di colonne poco correlate con il rating non aggiunge
# un gran potere discriminante

# punto 12
library(rpart)
albero = rpart(attitude$R ~ ., attitude[,2:7], method = "class" )
predict = predict(albero, attitude[,2:7], type="class")
cat("matrice di confusione con albero decisionale  \n")
print(table(attitude$R, predict))


# l'albero decisionale da risultati simili (un po' milgiori) di lda

