x <- runif(20)*10

#funzione che salva l'output in "test_1.pdf"
pdf("test.pdf")
hist(x, breaks=10)
#chiude il file "test" utilizzo per reindirizzare l'output di R e ritorna l'output al terminale
dev.off()