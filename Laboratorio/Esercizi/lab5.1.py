#! /usr/bin/python2
# -*- coding: utf-8 -*-

# Coded By Helias (Stefano Borzì)

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.graphics.api as smg

from matplotlib import pyplot as plt
from pandas.plotting import scatter_matrix
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from statsmodels.formula.api import ols
from statsmodels.datasets import get_rdataset

data = pd.read_csv('height_weight.csv')
print "Numero di record:",len(data)

data.boxplot(figsize=(8,6))
#plt.show()
plt.close()

scatter_matrix(data,figsize=(12,8))
#plt.show()
plt.close()

#Esercizio 1.1
x = np.random.normal(size=5000)
y = np.random.normal(size=5000)

plt.scatter(x, y)
#plt.show()
plt.close()


#Esercizio 1.2
print np.cov(data.height, data.weight)
print np.cov(data.height*2, data.weight*2)

print "\n"

#Esercizio 1.3
zscore = lambda x: (x-x.mean())/x.std(ddof=1) #utilizziamo lo stimatore corretto

z_h = zscore(data.height)
z_w = zscore(data.weight)

print np.cov(z_h, z_w)

# Domanda 1.5
print "Pearson: ",pearsonr(data.height,data.weight)


print "\n"

print "Spearman: ",spearmanr(data.height,data.weight)

print "\n"

#Esercizio 1.4
x = np.linspace(-10,10,100)
y = x*2 + 3 + np.random.normal(0,2,100) #modello lineare + rumore Gaussiano
#y[0] = -100 #aggiungo degli outliers
#y[-1] = 100
plt.scatter(x,y)
#plt.show()
plt.close()

print "Indice di correlazione di Pearson:",pearsonr(x,y)[0]
print "Indice di correlazione di Spearman:",spearmanr(x,y)[0]

######

model = ols("weight ~ height",data).fit()
model.summary()

sns.regplot('height','weight',data)
#plt.show()
plt.close()

print "Il regressore riduce l'errore sulle predizioni del %0.2f%%" % (model.rsquared*100)

print "R^2:",model.rsquared
print "Rho:",pearsonr(data.weight,data.height)[0]**2

######

fisher = get_rdataset('iris')
#mostriamo al documentazione
#print fisher.__doc__

sns.pairplot(fisher.data)
#plt.show()
plt.close()


corr_matrix = fisher.data.corr()
smg.plot_corr(corr_matrix, xnames=fisher.data.columns)
#plt.show()
plt.close()

sns.pairplot(fisher.data, kind='reg') #king='reg' specifica che vogliamoun plot di regressione
plt.show()


