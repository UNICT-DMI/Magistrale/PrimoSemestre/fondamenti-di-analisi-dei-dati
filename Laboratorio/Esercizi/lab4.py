#! /usr/bin/python2
# -*- coding: utf-8 -*-

#Coded By Helias

import pandas as pd
import numpy as np

from statsmodels.datasets import get_rdataset
from statsmodels.formula.api import ols # Ordinary Least Squares - OLS -> Metodo dei Minimi Quadrati
from matplotlib import pyplot as plt
from scipy.stats import pearsonr
from statsmodels.api import datasets
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split

cars = get_rdataset('cars')
#cars.data � il DataFrame contenente il dataset

#print "Numero di record:",len(cars.data)
#print cars.data.head()

#print cars.__doc__


plt.figure(figsize=(12,8))
plt.scatter(cars.data.speed,cars.data.dist)
plt.xlabel('Speed')
plt.ylabel('Dist')
#plt.show()
plt.close()

result = pearsonr(cars.data.speed,cars.data.dist) # restitusice due valori, coefficiente di Pearson e p-value
#print "Indice di correlazione di Pearson",result[0]
#print "p-value",result[1] # pi� il p-value � basso meglio �

#la notazione dist ~ speed indica che dist � la variabile
#dipendente e speed � la variabile indipendente
#i nomi delle variabili fanno riferimento al dataframe
#passato come secondo parametro
model = ols('dist ~ speed',cars.data).fit()
#visualizziamo i parametri appresi
model.params # intercept, speed, dtype

speed = 18
#print speed*model.params.speed + model.params.Intercept # restituisce la "dist" stimata in base alla speed

#print model.predict({'speed':[18]})

#print model.predict({'speed':[10,20,30]})

plt.figure(figsize=(12,8))
plt.scatter(cars.data.speed, cars.data.dist)
x1=np.min(cars.data.speed) #otteniamo gli estremi dei dati sull'asse x
x2=np.max(cars.data.speed)
y1,y2 = model.predict({'speed':[x1,x2]})
plt.plot([x1,x2],[y1,y2],'r')

#Esercizio 2.1
plt.scatter([10, 20, 30], model.predict({'speed':[10,20,30]}), color="g")

#plt.show()
plt.close()

#print "P-value del regressore: ",model.f_pvalue
#print "P.value dei signoli parametri:"
#print model.pvalues


#MSE -> Mean Standard Error
def stderr(predictions,targets):
    return np.sqrt(((predictions-targets)**2).mean())

#print "Std.Error",stderr(cars.data.dist,model.predict(cars.data))

boston = datasets.get_rdataset('Boston', package='MASS')
#print boston.__doc__
#print boston.data.head()

#costruiamo il modello
model = ols('medv ~ zn + indus + chas + nox + rm + age + dis + rad + tax + ptratio + black + lstat',boston.data).fit()
model.summary()

#Esercizio 3.1
print "MSE Boston: ",stderr(boston.data.medv, model.predict(boston.data))


model = ols('medv ~ rm + age',boston.data).fit()
model.summary()

#Esercizio 3.2
print "MSE Boston model2: ",stderr(boston.data.medv, model.predict(boston.data))


#generiamo 5 valori tra il minimo e il massimo dei dati presenti seugliassi x e y
x=np.linspace(boston.data.rm.min(),boston.data.rm.max(),5)
y=np.linspace(boston.data.age.min(),boston.data.age.max(),5)
#costruiamo una meshgrid per il plot 3D:
X,Y = np.meshgrid(x,y)
#otteniamo i valori predetti per ogni coppia x,y
#X e Y sono matrici bidimensionali, mentre la funzione predict
#lavoro solo su vettori monosimensionali
#dobbiamo dunque applicare l'operatore reshape(-1)
Z = model.predict({'rm':X.reshape(-1),'age':Y.reshape(-1)})
#adesso Z � un DataSeries
#per il plot, ci serve trasformarlo in una matrice bidimensionale
#della stessa forma di X o Y
Z=np.matrix(Z).reshape(X.shape)
fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot(111, projection='3d')
#plottiamo lo scatterplot 3D
ax.scatter(boston.data.rm,boston.data.age,boston.data.medv)
#plottiamo il piano di regressione
ax.plot_wireframe(X,Y,Z)
#plt.show()
plt.close()


training_set, test_set = train_test_split(boston.data,test_size=0.2)
print "Training set size:",len(training_set)
print "Test set size:",len(test_set)
#visualizziamo le prime righe del training set
training_set.head()

#generiamo 5 valori tra il minimo e il massimo dei dati presenti seugli assi x e y
x=np.linspace(boston.data.rm.min(),boston.data.rm.max(),5)
y=np.linspace(boston.data.age.min(),boston.data.age.max(),5)
#costruiamo una meshgrid per il plot 3D:
X,Y = np.meshgrid(x,y)
#otteniamo i valori predetti per ogni coppia x,y
#X e Y sono matrici bidimensionali, mentre la funzione predict
#lavoro solo su vettori monosimensionali
#dobbiamo dunque applicare l'operatore reshape(-1)
Z = model.predict({'rm':X.reshape(-1),'age':Y.reshape(-1)})
#adesso Z � un DataSeries
#per il plot, ci serve trasformarlo in una matrice bidimensionale
#della stessa forma di X o Y
Z=np.matrix(Z).reshape(X.shape)
fig = plt.figure(figsize=(16,6))
#facciamo un subplot 1 x 2
ax = fig.add_subplot(121, projection='3d')
#plottiamo lo scatterplot 3D
ax.scatter(training_set.rm,training_set.age,training_set.medv)
#plottiamo il piano di regressione
ax.plot_wireframe(X,Y,Z)
ax = fig.add_subplot(122, projection='3d')
#plottiamo lo scatterplot 3D
ax.scatter(test_set.rm,test_set.age,test_set.medv)
#plottiamo il piano di regressione
ax.plot_wireframe(X,Y,Z)
plt.show()
