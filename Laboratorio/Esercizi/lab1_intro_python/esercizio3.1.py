from matplotlib import pyplot as plt
import numpy as np

# esercizio 3.1
x = np.linspace(-2*np.pi, 2*np.pi, 50)
y = np.cos(x)

plt.figure()
plt.plot(x, y, "o-r")
plt.show()

