import numpy as np

# esercizio 2.1
matrix = np.array(([1,2,3],[5,6,7],[1,2,3],[1,2,3]))
print matrix

print "stampo prima riga"
print matrix[0]

print "\nstampo seconda colonna"
print matrix[:,1]

print "\n Somma della prima e dell' ultima colonna della matrice";
print (matrix[:,0]+matrix[:,-1])

print "\n Somma degli elementi della diagonale principale"
print matrix.diagonal().sum()

print "\n Numero di elementi della matrice"
print matrix.size

