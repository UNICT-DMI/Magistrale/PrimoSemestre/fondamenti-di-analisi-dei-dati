l = [1, 8, 2, 6, 15, 21, 76, 22, 0, 111, 23, 12, 24]

# primo elemento
print l[0]

# ultimo elemento
print l[-1]

# somma di tutti i valori con indice dispari l[1],l[3] ecc.
print sum(l[1::2]) # l[1:len(l):2]

# stampa lista ordinata in senso inverso
print l[::-1]

# media di tutti i valori della lista
print sum(l)/len(l)

